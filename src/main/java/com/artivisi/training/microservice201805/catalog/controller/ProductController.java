package com.artivisi.training.microservice201805.catalog.controller;

import com.artivisi.training.microservice201805.catalog.dao.ProductDao;
import com.artivisi.training.microservice201805.catalog.entity.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    @Autowired private ProductDao productDao;

    @GetMapping("/api/product/")
    public Iterable<Product> allProducts() {

        LOGGER.info("Mengambil data produk dari database");
        return productDao.findAll();
    }
}
