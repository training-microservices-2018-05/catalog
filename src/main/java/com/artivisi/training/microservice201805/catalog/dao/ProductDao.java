package com.artivisi.training.microservice201805.catalog.dao;

import com.artivisi.training.microservice201805.catalog.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {
}
